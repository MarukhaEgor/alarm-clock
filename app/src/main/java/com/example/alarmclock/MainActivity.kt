package com.example.alarmclock


import android.app.AlarmManager
import android.app.PendingIntent
import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import kotlinx.android.synthetic.main.activity_main.*
import java.util.*


class MainActivity : AppCompatActivity() {

    lateinit var my_intent: Intent
    lateinit var calendar: Calendar
    lateinit var pending_Intent: PendingIntent


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)


        button_set_alarm.setOnClickListener {
            onClickSetAlarm()
        }

        button_cancel_alarm.setOnClickListener {
            onClickStopAlarm()
        }
    }

    /**
     * setup alarm and show message about this alarm
     */

    fun onClickSetAlarm() {
        calendar = Calendar.getInstance()
        calendar.set(Calendar.HOUR_OF_DAY, timePicker.hour)
        calendar.set(Calendar.MINUTE, timePicker.minute)

        /* format time for message for user */
        var hour: String = timePicker.hour.toString()
        var minute: String = timePicker.minute.toString()

        if (timePicker.hour > 12) {
            hour = (timePicker.hour - 12).toString()
        }
        if (timePicker.minute < 10) {
            minute = "0" + timePicker.minute.toString()
        }
        val text = "$hour:$minute"

        Toast.makeText(applicationContext, "You set alarm at time " + text, Toast.LENGTH_LONG)
            .show()

        my_intent = Intent(applicationContext, AlarmReceiver::class.java)
        pending_Intent = PendingIntent.getBroadcast(
            applicationContext, 1, my_intent,
            PendingIntent.FLAG_UPDATE_CURRENT
        )

        val alarmManager =
            getSystemService(Context.ALARM_SERVICE) as AlarmManager

        alarmManager.setExactAndAllowWhileIdle(
            AlarmManager.RTC_WAKEUP,
            calendar.timeInMillis,
            pending_Intent
        )

    }

    /**
     * Delete pending intent of alarm and stop alarm
     */
    fun onClickStopAlarm() {

        val alarmManager =
            getSystemService(Context.ALARM_SERVICE) as AlarmManager
        my_intent = Intent(applicationContext, AlarmReceiver::class.java)
        pending_Intent = PendingIntent.getBroadcast(
            applicationContext, 1, my_intent,
            PendingIntent.FLAG_UPDATE_CURRENT
        )
        alarmManager.cancel(pending_Intent)
        my_intent.putExtra("alarm_state", "cancel_alarm")
        sendBroadcast(my_intent)

    }
}
